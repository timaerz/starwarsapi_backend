package com.example.demo;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = { "http://localhost:4200" })
@RestController
public class AuthenticationController {
    private User correctUser = new User("test@sample.com", "Check24!");
    private static HashMap<String, User> loggedInUsers = new HashMap<String, User>();
    private final int TokenLength = 40;

    private String generateToken() {
        String asciiUpperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String asciiLowerCase = asciiUpperCase.toLowerCase();
        String digits = "1234567890";
        String asciiChars = asciiUpperCase + asciiLowerCase + digits;

        StringBuilder sb = new StringBuilder();
        int i = 0;
        var rand = new Random();
        while (i < this.TokenLength) {
            sb.append(asciiChars.charAt(rand.nextInt(asciiChars.length())));
            i++;
        }
        return sb.toString();
    }

    @PostMapping(path = "/login")
    public ResponseEntity<Token> authenticate(@RequestBody User payload) {
        try {
            var realToken = new Token();
            var tempEmail = payload.getEmail();
            var tempPassword = payload.getPassword();
    
            if(tempEmail.equals(correctUser.getEmail()) && tempPassword.equals(correctUser.getPassword())) {
                final String token = this.generateToken();
                loggedInUsers.put(token, correctUser);
                realToken.setValue(token);
                return new ResponseEntity<>(realToken, HttpStatus.OK);
            }
    
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        } catch(Exception ex) {
            // TODO log exception
            return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
        }
    }   

    @PostMapping(path = "/logout")
    public ResponseEntity<Void> authenticate(@RequestHeader Map<String, String> headers) {
        try {
            var token = headers.get("Token");

            loggedInUsers.remove(token);
            return new ResponseEntity<Void>(HttpStatus.OK);
        } catch(Exception ex) {
            // TODO log exception
            return new ResponseEntity<Void>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }   
}
